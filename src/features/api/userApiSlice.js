import { apiSlice } from "./apiSlice";

export const userApiSlice = apiSlice.injectEndpoints({
  tagTypes: ["users", "user", "userProfile"],
  endpoints: (builder) => ({
    getUsers: builder.query({
      query: () => "/user",
      providesTags: ["users"],
    }),
    getUserById: builder.query({
      query: ({ id }) => `/user/${id}`,
      providesTags: ["user"],
    }),
    updateUser: builder.mutation({
      query: ({ id, body }) => ({
        url: `/user/${id}`,
        method: "PATCH",
        body: body,
      }),
      providesTags: ["userProfile"],
      // invalidatesTags: ["user"],
    }),
    uploadProfileImage: builder.mutation({
      query: (body) => ({
        url: "/user/profileImage",
        method: "POST",
        body: body,
      }),
      // providesTags: ["profileImage"],
      // invalidatesTags: ["user"],
    }),
  }),
});

export const {
  useGetUsersQuery,
  useLazyGetUserByIdQuery,
  useUpdateUserMutation,
  useUploadProfileImageMutation,
} = userApiSlice;
