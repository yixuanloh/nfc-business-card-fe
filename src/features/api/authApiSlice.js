import { apiSlice } from "./apiSlice";

export const authApiSlice = apiSlice.injectEndpoints({
  tagTypes: ["register", "login"],
  endpoints: (builder) => ({
    register: builder.mutation({
      query: (userData) => ({
        url: "/auth/register",
        method: "POST",
        body: userData,
      }),
      providesTags: ["register"],
      // invalidatesTags: ["user"],
    }),
    login: builder.mutation({
      query: (credentials) => ({
        url: "/auth/login",
        method: "POST",
        body: credentials,
      }),
      providesTags: ["login"],
      // invalidatesTags: ["user"],
    }),
  }),
});

export const { useRegisterMutation, useLoginMutation } = authApiSlice;
