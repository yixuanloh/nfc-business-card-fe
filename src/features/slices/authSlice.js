import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  token: null,
  id: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setCredentials: (state, action) => {
      const { token, id } = action.payload;
      state.token = token;
      state.id = id;
    },
    logout: (state, action) => {
      state.token = null;
      state.id = null;
    },
  },
});

export const { setCredentials, logout } = authSlice.actions;

export default authSlice.reducer;

export const selectCurrentUser = (state) => state.auth.user;
export const selectCurrentToken = (state) => state.auth.token;
export const selectCurrentId = (state) => state.auth.id;
