import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ThemeProvider, CssBaseline } from "@mui/material";
import { Toaster } from "react-hot-toast";

import theme from "./utils/theme";
import Layout from "./components/Layout";
import Profile from "./pages/Profile";
import Login from "./pages/Login";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Toaster />
      <CssBaseline />
      <BrowserRouter>
        <Routes>
          <Route element={<Layout />}>
            <Route index path="/" element={<Login />} />
            <Route path="/profile/:id" exact element={<Profile />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
