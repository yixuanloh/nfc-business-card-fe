import { createTheme } from "@mui/material";

const theme = createTheme({
  typography: {
    fontFamily: "Roboto, sans-serif",
  },
  components: {
    MuiTextField: {
      styleOverrides: {
        root: {
          "& .MuiOutlinedInput-root": {
            "&.Mui-focused fieldset": {
              borderColor: "grey",
            },
          },
          "& .MuiInputLabel-root": {
            "&.Mui-focused": {
              color: "grey",
            },
          },
        },
      },
    },
  },
  palette: {
    primary: {
      main: "#F0F0F0",
    },
    secondary: {
      main: "#000000",
    },
  },
});

export default theme;
