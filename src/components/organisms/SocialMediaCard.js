import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Card, CardContent, Grid, useTheme, Box } from "@mui/material";
import {
  LinkedIn,
  Instagram,
  Facebook,
  Twitter,
  Reddit,
  Add,
  WhatsApp,
  Directions,
  AddIcCall,
  Email,
} from "@mui/icons-material";
import toast from "react-hot-toast";
import CircleIcon from "../atoms/CircleIcon";
import SocialMediaModal from "./SocialMediaModal";

const iconList = [
  {
    id: "linkedin",
    icon: <LinkedIn />,
  },
  {
    id: "facebook",
    icon: <Facebook />,
  },
  {
    id: "instagram",
    icon: <Instagram />,
  },
  {
    id: "twitter",
    icon: <Twitter />,
  },
  {
    id: "whatsapp",
    icon: <WhatsApp />,
  },
  {
    id: "maps",
    icon: <Directions />,
  },
  {
    id: "contact",
    icon: <AddIcCall />,
  },
  {
    id: "email",
    icon: <Email />,
  },
];

const SocialMediaCard = ({ userData, setUserData, isEditing, urls }) => {
  const theme = useTheme();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [socialMedia, setSocialMedia] = useState(null);
  const [url, setUrl] = useState(null);
  const [formData, setFormData] = useState([]);

  useEffect(() => {
    setFormData([...urls]);
  }, [urls]);

  useEffect(() => {
    if (formData.length !== urls.length) {
      setFormData([...urls]);
    }
  }, [isEditing]);

  const handleLinkClick = (icon, url, index) => {
    let newUrl = url;
    if (icon === "contact") {
      newUrl = `tel:${url}`;
    }
    if (icon === "email") {
      newUrl = `mailto:${url}`;
    }
    if (isEditing) {
      let updatedFormData = [...formData];
      updatedFormData.splice(index, 1);
      setFormData(updatedFormData);
      setUserData({
        ...userData,
        socialMediaSection: updatedFormData,
      });
    } else {
      const supportsCustomScheme =
        window.navigator && window.navigator.msLaunchUri;
      if (supportsCustomScheme) {
        // Use the custom URL scheme to launch the app
        const customUrl = `myapp://${encodeURIComponent(newUrl)}`;
        window.navigator.msLaunchUri(
          customUrl,
          () => {},
          () => {}
        );
      } else {
        // Fall back to opening the link in a new browser tab
        window.open(newUrl, "_blank");
      }
    }
  };

  const handleSave = () => {
    let isDuplicate = formData.find((icon) => icon.icon === socialMedia);

    if (socialMedia && url && !isDuplicate) {
      let updatedSocialMediaSection = [...formData];
      updatedSocialMediaSection.push({ icon: socialMedia, url: url });
      setFormData(updatedSocialMediaSection);
      setUserData({
        ...userData,
        socialMediaSection: updatedSocialMediaSection,
      });
      setIsModalOpen(!isModalOpen);
    } else {
      toast(
        isDuplicate
          ? `${socialMedia} is already exist`
          : `Please choose a shortcut`,
        {
          icon: "ℹ️",
        }
      );
    }
  };

  return (
    <>
      <SocialMediaModal
        socialMedia={socialMedia}
        setSocialMedia={setSocialMedia}
        url={url}
        setUrl={setUrl}
        handleSave={handleSave}
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
      />
      <Box
        sx={{
          display: "flex",
          borderRadius: 4,
          mt: 3,
          p: 2,
          backgroundColor: theme.palette.primary.main,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {/* <CardContent> */}
        <Grid container spacing={3}>
          {formData?.map((url, index) => (
            <Grid
              key={url.icon}
              item
              xs={3}
              display="flex"
              justifyContent="center"
              alignItems="center"
              onClick={() => handleLinkClick(url.icon, url.url, index)}
            >
              {iconList.find((icon) => icon.id === url.icon) && (
                <CircleIcon
                  icon={iconList.find((icon) => icon.id === url.icon).icon}
                  isEditing={isEditing}
                />
              )}
            </Grid>
          ))}
          {isEditing && (
            <Grid
              item
              xs={3}
              display="flex"
              justifyContent="center"
              alignItems="center"
              onClick={() => setIsModalOpen(!isModalOpen)}
            >
              <CircleIcon icon={<Add />} />
            </Grid>
          )}
        </Grid>
        {/* </CardContent> */}
      </Box>
    </>
  );
};

SocialMediaCard.propTypes = {
  urls: PropTypes.array,
  isEditing: PropTypes.bool,
  userData: PropTypes.object,
  setUserData: PropTypes.func,
};

SocialMediaCard.defaultProps = {
  urls: [],
  isEditing: false,
  userData: {},
  setUserData: () => null,
};

export default SocialMediaCard;
