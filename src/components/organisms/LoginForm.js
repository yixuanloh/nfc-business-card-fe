import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  Box,
  Button,
  Backdrop,
  CircularProgress,
  useTheme,
  Typography,
} from "@mui/material";
import {
  selectCurrentToken,
  setCredentials,
  selectCurrentId,
} from "../../features/slices/authSlice";
import { useLoginMutation } from "../../features/api/authApiSlice";
import TextField from "../molecules/TextField";
import * as yup from "yup";
import toast from "react-hot-toast";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

const schema = yup
  .object({
    username: yup.string().required("Username is required."),
    password: yup.string().required("Password is required."),
  })
  .required();

const LoginForm = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const token = useSelector(selectCurrentToken);
  const userId = useSelector(selectCurrentId);
  const methods = useForm({
    resolver: yupResolver(schema),
  });

  const { handleSubmit } = methods;

  const [login, { data, isLoading, isError, isSuccess, error, reset }] =
    useLoginMutation();

  const onSubmit = async (data) => {
    try {
      const userData = await login(data).unwrap();
      dispatch(setCredentials({ ...userData }));
      toast.success("Login Successfully");
      navigate(`/profile/${userData?.id}`);
    } catch (error) {
      toast.error(error.data.message);
    }
  };

  useEffect(() => {
    if (token) {
      navigate(`/profile/${userId}`);
    }
  }, []);

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      {isLoading && !isSuccess ? (
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={true}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      ) : null}
      <FormProvider {...methods}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box display="flex" flexDirection="column">
            <Box>
              <TextField name="username" label="Username" size="small" />
            </Box>
            <Box mt={3}>
              <TextField
                name="password"
                label="Password"
                type="password"
                size="small"
              />
            </Box>
            <Button
              sx={{
                mt: 3,
                px: 0,
                backgroundColor: "black",
                color: "white",
                "&:hover": {
                  backgroundColor: "black",
                },
              }}
              disableRipple
              variant="contained"
              type="submit"
            >
              Login
            </Button>
            {/* <Box mt={3}>
              <Link to="/register">Signup now</Link>
            </Box> */}
          </Box>
        </form>
      </FormProvider>
    </Box>
  );
};

export default LoginForm;
