import React, { useEffect, useState } from "react";
import { useForm, FormProvider } from "react-hook-form";
import PropTypes from "prop-types";
import {
  Card,
  CardContent,
  Box,
  CardMedia,
  Typography,
  useTheme,
  Grid,
} from "@mui/material";
import TextField from "../molecules/TextField";

const AboutCard = ({
  userData,
  setUserData,
  isEditing,
  title,
  description,
}) => {
  const methods = useForm();
  const theme = useTheme();
  const [formData, setFormData] = useState({});

  useEffect(() => {
    setFormData({
      title: title,
      description: description,
    });
  }, [title, description]);

  const handleChange = (name, value) => {
    let updatedAboutSection = {
      ...formData,
      [name]: value,
    };
    setFormData({ ...updatedAboutSection });
    setUserData({
      ...userData,
      aboutSection: updatedAboutSection,
    });
  };

  return (
    <>
      <Card
        sx={{
          display: "flex",
          flexDirection: "column",
          borderRadius: 4,
          backgroundColor: theme.palette.primary.main,
        }}
      >
        <CardContent
          sx={{
            // backgroundColor: theme.palette.primary.main,
            display: "flex",
            flexDirection: "column",
            textAlign: "center",
          }}
        >
          {isEditing ? (
            <FormProvider {...methods}>
              <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
                <TextField
                  name="title"
                  label="Title"
                  type="text"
                  size="small"
                  defaultValue={title}
                  onChange={(e) => handleChange("title", e.target.value)}
                />

                <TextField
                  name="description"
                  label="Description"
                  type="text"
                  size="small"
                  defaultValue={description}
                  onChange={(e) => handleChange("description", e.target.value)}
                />
              </Box>
            </FormProvider>
          ) : (
            <>
              <Typography
                fontSize={22}
                fontWeight={600}
                sx={{ wordWrap: "break-word" }}

                // color={theme.palette.primary.main}
              >
                {title}
              </Typography>
              <Typography letterSpacing={2} sx={{ wordWrap: "break-word" }}>
                {description}
              </Typography>
            </>
          )}
        </CardContent>
      </Card>
    </>
  );
};

AboutCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  isEditing: PropTypes.bool,
  userData: PropTypes.object,
  setUserData: PropTypes.func,
};

AboutCard.defaultProps = {
  title: "",
  description: "",
  isEditing: false,
  userData: {},
  setUserData: () => null,
};

export default AboutCard;
