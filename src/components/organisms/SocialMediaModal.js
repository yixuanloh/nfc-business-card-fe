import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import SocialMediaSelector from "../molecules/SocialMediaSelector";

const SocialMediaModal = ({
  socialMedia,
  setSocialMedia,
  url,
  setUrl,
  handleSave,
  isModalOpen,
  setIsModalOpen,
}) => {
  const handleClose = () => {
    setIsModalOpen(false);
  };
  return (
    <Dialog open={isModalOpen} onClose={handleClose} fullWidth>
      <DialogTitle>Select Shortcut</DialogTitle>
      <DialogContent
        sx={{
          pt: "24px !important",
        }}
      >
        <SocialMediaSelector
          socialMedia={socialMedia}
          setSocialMedia={setSocialMedia}
          url={url}
          setUrl={setUrl}
          handleSave={handleSave}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleSave}>
          <Typography color="black" fontSize={14}>
            Add
          </Typography>
        </Button>
      </DialogActions>
    </Dialog>
  );
};

SocialMediaModal.propTypes = {
  isModalOpen: PropTypes.bool,
  setIsModalOpen: PropTypes.func,
  socialMedia: PropTypes.string,
  setSocialMedia: PropTypes.func,
  url: PropTypes.string,
  setUrl: PropTypes.func,
  handleSave: PropTypes.func,
};

SocialMediaModal.defaultProps = {
  isModalOpen: false,
  setIsModalOpen: () => null,
  socialMedia: "",
  setSocialMedia: () => null,
  url: "",
  setUrl: () => null,
  handleSave: () => null,
};

export default SocialMediaModal;
