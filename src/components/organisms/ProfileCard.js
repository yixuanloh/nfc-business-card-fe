import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useForm, FormProvider } from "react-hook-form";
import {
  Card,
  CardContent,
  Box,
  CardMedia,
  Typography,
  useTheme,
} from "@mui/material";
import { AddCircle } from "@mui/icons-material";
import TextField from "../molecules/TextField";

const ProfileCard = ({
  name,
  title,
  profileImageUrl,
  isEditing,
  userData,
  setUserData,
}) => {
  const theme = useTheme();
  const methods = useForm();
  const [imagePreview, setImagePreview] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const [formData, setFormData] = useState({});

  useEffect(() => {
    setFormData({
      name: name,
      title: title,
    });
  }, [name, title]);

  useEffect(() => {
    if (!isEditing) {
      setImagePreview(null);
    }
  }, [isEditing]);

  const handleImageUpload = (file) => {
    setImageUrl(file);
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImagePreview(reader.result);
    };
  };

  useEffect(() => {
    setUserData({
      ...userData,
      profileImageUrl: imageUrl,
    });
  }, [imageUrl]);

  const handleChange = (name, value) => {
    let updatedProfileSection = {
      ...formData,
      [name]: value,
    };
    setFormData({ ...updatedProfileSection });
    setUserData({
      ...userData,
      profileSection: updatedProfileSection,
    });
  };

  return (
    <>
      <Card
        sx={{
          display: "flex",
          borderRadius: 4,
          height: 200,
        }}
      >
        <Box sx={{ display: "flex", position: "relative", flex: 1 }}>
          {isEditing ? (
            <>
              <CardMedia
                component="img"
                image={imagePreview || profileImageUrl}
                sx={{ flex: 1 }}
              />

              <input
                accept="image/*"
                type="file"
                id="upload-image"
                style={{ display: "none" }}
                onChange={(e) => handleImageUpload(e.target.files[0])}
              />
              <label htmlFor="upload-image">
                <Box
                  sx={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                >
                  <AddCircle
                    sx={{ color: "lightgrey", width: "60px", height: "60px" }}
                  />
                </Box>
              </label>
            </>
          ) : (
            <CardMedia
              component="img"
              image={profileImageUrl}
              sx={{ flex: 1 }}
            />
          )}
        </Box>

        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            flex: 1.3,
          }}
        >
          <CardContent
            sx={{
              flex: 1,
              backgroundColor: theme.palette.primary.main,
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
            }}
          >
            {isEditing ? (
              <FormProvider {...methods}>
                <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
                  <TextField
                    name="name"
                    label="Name"
                    type="text"
                    size="small"
                    defaultValue={name}
                    onChange={(e) => handleChange("name", e.target.value)}
                  />
                  <TextField
                    name="title"
                    label="Title"
                    type="text"
                    size="small"
                    defaultValue={title}
                    onChange={(e) => handleChange("title", e.target.value)}
                  />
                </Box>
              </FormProvider>
            ) : (
              <>
                <Typography variant="h5" fontWeight={600}>
                  {name}
                </Typography>
                <Typography
                  variant="subtitle1"
                  component="div"
                  letterSpacing={2}
                >
                  {title}
                </Typography>
              </>
            )}
          </CardContent>
        </Box>
      </Card>
    </>
  );
};

ProfileCard.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  profileImage: PropTypes.string,
  isEditing: PropTypes.bool,
  userData: PropTypes.object,
  setUserData: PropTypes.func,
};

ProfileCard.defaultProps = {
  name: "",
  title: "",
  profileImage: "",
  isEditing: false,
  userData: {},
  setUserData: () => null,
};

export default ProfileCard;
