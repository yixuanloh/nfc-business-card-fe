import React, { useEffect, useState } from "react";
import { useForm, FormProvider } from "react-hook-form";
import PropTypes from "prop-types";
import {
  Card,
  CardContent,
  Box,
  CardMedia,
  Typography,
  useTheme,
  Grid,
  Divider,
} from "@mui/material";
import TextField from "../molecules/TextField";

const ContactUs = ({ userData, setUserData, isEditing, contacts }) => {
  const methods = useForm();
  const theme = useTheme();
  const [formData, setFormData] = useState({});

  useEffect(() => {
    setFormData({
      ...contacts,
    });
  }, [contacts]);

  const handleChange = (name, value, index) => {
    let updatedContactSection = {};
    if (isNaN(index) && name === "title") {
      updatedContactSection = {
        ...formData,
        title: value,
      };
      setFormData({ ...updatedContactSection });
    } else {
      const contactsCopy = [...formData?.contacts];
      contactsCopy[index] = {
        ...contactsCopy[index],
        [name]: value,
      };
      updatedContactSection = {
        ...formData,
        contacts: contactsCopy,
      };
      setFormData({ ...updatedContactSection });
    }
    setUserData({
      ...userData,
      contactSection: updatedContactSection,
    });
  };

  return (
    <>
      <Card
        sx={{
          display: "flex",
          flexDirection: "column",
          borderRadius: 4,
        }}
      >
        <CardContent
          sx={{
            // flex: "1 0 auto",
            backgroundColor: theme.palette.primary.main,
            display: "flex",
            flexDirection: "column",
          }}
        >
          {isEditing ? (
            <FormProvider {...methods}>
              <TextField
                name="title"
                label="Title"
                type="text"
                size="small"
                defaultValue={contacts?.title}
                onChange={(e) => handleChange("title", e.target.value)}
              />
              <Divider sx={{ pb: 2 }} />
              {contacts?.contacts?.map((contact, index) => (
                <Box
                  key={contact.title}
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 2,
                    pt: index === 0 ? 2 : 4,
                  }}
                >
                  <TextField
                    name="title"
                    label="Title"
                    type="text"
                    size="small"
                    defaultValue={contact?.title}
                    onChange={(e) =>
                      handleChange("title", e.target.value, index)
                    }
                  />
                  <TextField
                    name="description"
                    label="Description"
                    type="text"
                    size="small"
                    defaultValue={contact?.description}
                    onChange={(e) =>
                      handleChange("description", e.target.value, index)
                    }
                  />
                </Box>
              ))}
            </FormProvider>
          ) : (
            <>
              <Typography
                fontSize={22}
                fontWeight={600}
                sx={{ wordWrap: "break-word" }}
              >
                {contacts?.title}
              </Typography>
              <Divider sx={{ pb: 2 }} />
              {contacts?.contacts?.map((contact) => (
                <Box key={contact.title} pt={2}>
                  <Typography
                    fontSize={18}
                    fontWeight={600}
                    sx={{ wordWrap: "break-word" }}
                  >
                    {contact.title}
                  </Typography>
                  <Typography letterSpacing={2} sx={{ wordWrap: "break-word" }}>
                    {contact.description}
                  </Typography>
                </Box>
              ))}
            </>
          )}
        </CardContent>
      </Card>
    </>
  );
};

ContactUs.propTypes = {
  contacts: PropTypes.object,
  isEditing: PropTypes.bool,
  userData: PropTypes.object,
  setUserData: PropTypes.func,
};

ContactUs.defaultProps = {
  contacts: {},
  isEditing: false,
  userData: {},
  setUserData: () => null,
};

export default ContactUs;
