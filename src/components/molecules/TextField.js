import AtomTextField from "../atoms/AtomTextField";
import { useFormContext, Controller } from "react-hook-form";

const TextField = ({ name, ...textFieldProps }) => {
  const { control } = useFormContext();
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <AtomTextField
          onChange={onChange}
          value={value}
          error={!!error}
          helperText={error ? error.message : null}
          {...textFieldProps}
        />
      )}
    />
  );
};

export default TextField;
