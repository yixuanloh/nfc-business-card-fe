import React from "react";
import PropTypes from "prop-types";
import { FormControl, MenuItem } from "@mui/material";
import {
  LinkedIn,
  Instagram,
  Facebook,
  Twitter,
  WhatsApp,
  Directions,
  AddIcCall,
  Email,
} from "@mui/icons-material";
import AtomTextField from "../atoms/AtomTextField";

const socialMediaOptions = [
  { value: "facebook", label: "Facebook", icon: <Facebook /> },
  { value: "twitter", label: "Twitter", icon: <Twitter /> },
  { value: "instagram", label: "Instagram", icon: <Instagram /> },
  { value: "linkedin", label: "LinkedIn", icon: <LinkedIn /> },
  { value: "whatsapp", label: "Whatsapp", icon: <WhatsApp /> },
  { value: "maps", label: "Maps", icon: <Directions /> },
  { value: "contact", label: "Contact", icon: <AddIcCall /> },
  { value: "email", label: "Email", icon: <Email /> },
];

const SocialMediaSelector = ({ socialMedia, setSocialMedia, url, setUrl }) => {
  const handleSocialMediaChange = (event) => {
    setSocialMedia(event.target.value);
  };

  const handleUrlChange = (event) => {
    setUrl(event.target.value);
  };

  return (
    <FormControl fullWidth size="small" sx={{ gap: 2 }}>
      {/* <InputLabel id="social-media-label">Social Media</InputLabel> */}
      <AtomTextField
        select
        labelId="social-media-label"
        id="social-media"
        label="Shortcut"
        value={socialMedia}
        onChange={handleSocialMediaChange}
        displayEmpty
        size="small"
      >
        {socialMediaOptions.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </AtomTextField>
      <AtomTextField
        label="Url"
        variant="outlined"
        value={url}
        onChange={handleUrlChange}
        size="small"
      />
    </FormControl>
  );
};

SocialMediaSelector.propTypes = {
  onChange: PropTypes.func,
  socialMedia: PropTypes.string,
  setSocialMedia: PropTypes.func,
  url: PropTypes.string,
  setUrl: PropTypes.func,
};

SocialMediaSelector.defaultProps = {
  onChange: () => null,
  socialMedia: "",
  setSocialMedia: () => null,
  url: "",
  setUrl: () => null,
};

export default SocialMediaSelector;
