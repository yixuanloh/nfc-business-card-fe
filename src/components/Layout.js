import React from "react";
import { Outlet, useLocation } from "react-router-dom";
import { Box, useTheme } from "@mui/material";

const Layout = () => {
  const theme = useTheme();
  const location = useLocation();
  return (
    <Box
      display="flex"
      flexDirection="column"
      minHeight="100vh"
      sx={{
        backgroundColor:
          location.pathname !== "/" ? theme.palette.secondary.main : null,
      }}
    >
      <Box flex={1}>
        <Outlet />
      </Box>
    </Box>
  );
};

export default Layout;
