import React from "react";
import PropTypes from "prop-types";
import { Box, IconButton } from "@mui/material";
import { Delete } from "@mui/icons-material";

const CircleIcon = ({ icon, isEditing }) => {
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: 50,
        height: 50,
        cursor: "pointer",
        position: "relative",
      }}
    >
      {icon}
      {isEditing && (
        <IconButton
          size="small"
          sx={{
            position: "absolute",
            top: -12,
            right: -12,
          }}
          onClick={null}
        >
          <Delete />
        </IconButton>
      )}
    </Box>
  );
};

CircleIcon.propTypes = {
  isEditing: PropTypes.bool,
};

CircleIcon.defaultProps = {
  isEditing: false,
};

export default CircleIcon;
