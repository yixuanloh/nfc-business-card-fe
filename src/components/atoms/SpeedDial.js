import React, { useState } from "react";
import PropTypes from "prop-types";
import { SpeedDial as MuiSpeedDial, SpeedDialAction } from "@mui/material";

import SpeedDialIcon from "@mui/material/SpeedDialIcon";
import { MoreHoriz, Close } from "@mui/icons-material";

const SpeedDial = ({ actions }) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <MuiSpeedDial
      ariaLabel="SpeedDial controlled open example"
      sx={{
        position: "fixed",
        bottom: 16,
        right: 16,
      }}
      icon={<SpeedDialIcon sx={{ color: "white" }} openIcon={<Close />} />}
      FabProps={{
        sx: {
          backgroundColor: "black",
          "&:hover": {
            backgroundColor: "black",
          },
        },
      }}
      onClose={handleClose}
      onOpen={handleOpen}
      open={open}
    >
      {actions.map((action) => (
        <SpeedDialAction
          key={action.name}
          icon={action.icon}
          tooltipTitle={action.name}
          onClick={action.onClick}
          sx={{
            backgroundColor: "black",
            color: "white",
          }}
        />
      ))}
    </MuiSpeedDial>
  );
};

SpeedDial.propTypes = {
  actions: PropTypes.array,
};

SpeedDial.defaultProps = {
  actions: [],
};

export default SpeedDial;
