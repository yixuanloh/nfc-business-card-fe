import { TextField as MuiTextField } from "@mui/material";

const AtomTextField = ({ ...props }) => {
  return (
    <MuiTextField
      data-testid="textfield"
      id="textfield"
      label="Label"
      variant="outlined"
      {...props}
    />
  );
};

export default AtomTextField;
