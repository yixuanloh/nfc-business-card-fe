import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";
import {
  Container,
  useTheme,
  Box,
  CardMedia,
  Backdrop,
  CircularProgress,
  Fab,
} from "@mui/material";
import { Edit, Logout, Share, Done, Clear } from "@mui/icons-material";
import toast from "react-hot-toast";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import {
  useGetUsersQuery,
  useLazyGetUserByIdQuery,
  useUpdateUserMutation,
  useUploadProfileImageMutation,
} from "../features/api/userApiSlice";
import jwt_decode from "jwt-decode";
import { selectCurrentToken, logout } from "../features/slices/authSlice";
import ProfileCard from "../components/organisms/ProfileCard";
import AboutCard from "../components/organisms/AboutCard";
import ContactUs from "../components/organisms/ContactUsCard";
import SpeedDial from "../components/atoms/SpeedDial";
import SocialMediaCard from "../components/organisms/SocialMediaCard";

const Profile = () => {
  const theme = useTheme();
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const token = useSelector(selectCurrentToken);
  const [actions, setActions] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [userData, setUserData] = useState(null);
  const [
    getUserById,
    {
      data: user,
      isFetching: isUserFetching,
      isLoading: isUserLoading,
      isSuccess: isUserSuccess,
    },
  ] = useLazyGetUserByIdQuery();

  const [
    uploadProfileImage,
    {
      data: profileImage,
      isLoading: isProfileImageLoading,
      isSuccess: isProfileImageSuccess,
    },
  ] = useUploadProfileImageMutation();

  const [
    updateUser,
    {
      data: updatedUser,
      isLoading: isUpdatedUserLoading,
      isSuccess: isUpdatedUserSuccess,
    },
  ] = useUpdateUserMutation();

  const handleShareClick = () => {
    if (navigator.share) {
      navigator.share({
        url: window.location.href,
      });
    }
  };

  const handleSubmit = async () => {
    if (userData) {
      let url = {};
      if (userData?.profileImageUrl) {
        let formData = new FormData();
        formData.append("file", userData.profileImageUrl);
        formData.append("userId", id);

        url = await uploadProfileImage(formData).unwrap();
      }
      try {
        const user = await updateUser({
          id: id,
          body: {
            ...userData,
            profileImageUrl: url
              ? url.profileImageUrl
              : userData.profileImageUrl,
          },
        }).unwrap();
        if (user) {
          toast.success("Updated");
        }
      } catch (error) {
        toast.error(error.data.message);
      }
    }
    setIsEditing(!isEditing);
  };

  const handleEdit = () => {
    setIsEditing(!isEditing);
  };

  const handleLogout = () => {
    dispatch(logout());
  };

  useEffect(() => {
    if (token) {
      const decodedToken = jwt_decode(token);
      const currentTime = Date.now() / 1000; // convert to seconds

      if (decodedToken.exp < currentTime) {
        // Redirect to the login page if the token is expired
        dispatch(logout());
        navigate("/");
      }
    }
  }, []);

  useEffect(() => {
    if (token) {
      setActions([
        { icon: <Share />, name: "Share", onClick: () => handleShareClick() },
        {
          icon: <Edit />,
          name: "Edit",
          onClick: () => handleEdit(),
        },
        {
          icon: <Logout />,
          name: "Logout",
          onClick: () => {
            handleLogout();
            toast.success("Logout Successfully");
            navigate("/");
          },
        },
      ]);
    } else {
      setActions([
        { icon: <Share />, name: "Share", onClick: () => handleShareClick() },
      ]);
    }
    if (id) {
      getUserById({ id: id });
    }
  }, [id, isEditing]);

  useEffect(() => {
    setUserData(null);
  }, [isEditing]);

  return (
    <Container>
      {(isUpdatedUserLoading && !isUpdatedUserSuccess) ||
      (isProfileImageLoading && !isProfileImageSuccess) ? (
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={true}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      ) : null}
      {/* TODO: Make it into atom component */}
      {(isUserFetching || isUserLoading) && !isUserSuccess ? (
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={true}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      ) : null}
      <Box mt={4} mx={3} justifyContent="center">
        <CardMedia
          component="img"
          image="/winson.jpeg"
          // alt="Live from space album cover"
        />
      </Box>
      <Box my={3}>
        <ProfileCard
          userData={userData}
          setUserData={setUserData}
          isEditing={isEditing}
          name={user?.profileSection?.name}
          title={user?.profileSection?.title}
          profileImageUrl={user?.profileImageUrl}
        />
      </Box>
      <Box my={3}>
        <SocialMediaCard
          userData={userData}
          setUserData={setUserData}
          isEditing={isEditing}
          urls={user?.socialMediaSection}
        />
      </Box>
      <Box my={3}>
        <AboutCard
          userData={userData}
          setUserData={setUserData}
          isEditing={isEditing}
          title={user?.aboutSection?.title}
          description={user?.aboutSection?.description}
        />
      </Box>
      {/* <Box my={3}>
        <Swiper
          modules={[Navigation, Pagination, Scrollbar, A11y]}
          spaceBetween={50}
          slidesPerView={1}
          pagination={true}
          style={{
            "--swiper-pagination-color": theme.palette.primary.main,
          }}
        >
          <SwiperSlide>
            <CardMedia
              sx={{ borderRadius: 4 }}
              component="img"
              image="https://i.pravatar.cc/123"
            />
          </SwiperSlide>
          <SwiperSlide>
            <CardMedia
              sx={{ borderRadius: 4 }}
              component="img"
              image="https://i.pravatar.cc/242"
            />
          </SwiperSlide>
          <SwiperSlide>
            <CardMedia
              sx={{ borderRadius: 4 }}
              component="img"
              image="https://i.pravatar.cc/433"
            />
          </SwiperSlide>
          <SwiperSlide>
            <CardMedia
              sx={{ borderRadius: 4 }}
              component="img"
              image="https://i.pravatar.cc/4124"
            />
          </SwiperSlide>
        </Swiper>
      </Box> */}
      <Box my={3}>
        <ContactUs
          userData={userData}
          setUserData={setUserData}
          isEditing={isEditing}
          contacts={user?.contactSection}
        />
      </Box>
      {isEditing ? (
        <>
          {/* TODO: Refactor fab component */}
          <Box
            sx={{ position: "fixed", bottom: 16, right: 16, display: "flex" }}
          >
            <Box mr={1}>
              <Fab
                sx={{ color: "white", backgroundColor: "black" }}
                onClick={handleSubmit}
              >
                <Done />
              </Fab>
            </Box>
            <Box>
              <Fab
                sx={{ color: "black", backgroundColor: "white" }}
                onClick={handleEdit}
              >
                <Clear />
              </Fab>
            </Box>
          </Box>
        </>
      ) : (
        <SpeedDial actions={actions} />
      )}
    </Container>
  );
};

export default Profile;
